var express     = require('express');
var http        = require('http');
var path        = require('path');
var favicon     = require('static-favicon');
var logger      = require('morgan');
var passport    = require('passport')
var cors        = require('cors')

var bodyParser  = require('body-parser');
var oauth2orize = require('oauth2orize');
var server      = oauth2orize.createServer();


var config      = require('./libs/config');
var log         = require('./libs/log')(module);
var oauth2      = require('./libs/oauth2');

var libs = process.cwd() + '/libs/';
require(libs + 'auth');

var cookieParser = require('cookie-parser');
// var bodyParser   = require('body-parser');




var routes  = require('./routes');
var users   = require('./routes/user');
var api     = require('./routes/api');
var phone   = require('./routes/phone');

var app = express();

//For BodyParser

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.options('*', cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(app.router);

app.get('/', routes.index);
// app.get('/users', users.list);
// app.get('/api', api.main);



app.get('/api/info', passport.authenticate('bearer', { session: false }), users.userinfo );
app.post('/api/oauth', cors(), oauth2.token);


app.post('/api/phone', cors(), passport.authenticate('bearer', { session: false }), phone.checknum);
app.put('/api/phone', cors(), passport.authenticate('bearer', { session: false }), phone.putnum);
app.delete('/api/phone', cors(), passport.authenticate('bearer', { session: false }), phone.delnum );
app.post('/api/getphones', cors(), passport.authenticate('bearer', { session: false }), phone.getall );


/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    log.debug('Not found URL: %s',req.url);
    next(err);
});


app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});


app.get('/ErrorExample', function(req, res, next){
    next(new Error('Random error!'));
});


/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
