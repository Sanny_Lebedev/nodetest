
var passport               = require('passport');
var BasicStrategy          = require('passport-http').BasicStrategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
var BearerStrategy         = require('passport-http-bearer').Strategy;
// var Sequelize              = require('sequelize');

var config = require('./config');
var log    = require('./log')(module);

var models       = require('../models');
var AccessToken  = models.AccessToken;
var RefreshToken = models.RefreshToken;


passport.use(new BasicStrategy(
    
    function(username, password, done) {
        log.debug('YES');

        User.findOne({ clientId: username }, function(err, client) {
            if (err) { 
            	return done(err); 
            }

            if (!client) { 
            	return done(null, false); 
            }

            if (client.clientSecret !== password) { 
            	return done(null, false); 
            }

            return done(null, client);
        });    
    }

));

passport.use(new ClientPasswordStrategy(
    function(clientId, clientSecret, done) {
       
        models.client.findOne({where: {clientId: clientId}})
        .then(function(client) {
        if (!client) {
          console.log('No users has been found.');
          return done(null, false); 
        } else {
          console.log('Hello ' + client.id + '!');
          if (client.clientSecret !== clientSecret) { 
            return done(null, false); 
        }
        return done(null, client);
        }
      })
      .catch(function(err) {
        // Error handling here
        return done(err);
      });

    }

));

passport.use(new BearerStrategy(
    function(accessToken, done) {


        AccessToken.findOne({where: {token: accessToken}})
        .then(function(token) {
        if (!token) {
            return done(null, false); 
        } else {
            if( Math.round((Date.now()-token.created)/1000) > config.get('security:tokenLife') ) {

                AccessToken.destroy({ where: { token: accessToken }})
                    .catch(function (error){
                        return done(error);
                    });
            

                return done(null, false, { message: 'Token expired' });
            }

           

            models.users.findOne({where: {id: parseInt(token.userID)}})
            .then(function(user) {
            if (!user) {
                return done(null, false, { message: 'Unknown user' }); 
            } else {
                var info = { scope: '*' };
                done(null, user, info);
            }
          })
          .catch(function(err) {
            return done(err)
          });
        }
      })
      .catch(function(err) {
        return done(err); 
      });

     
    }
));