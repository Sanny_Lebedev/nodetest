var oauth2orize = require('oauth2orize');
var passport 	= require('passport');
var crypto 		= require('crypto');

var config      = require('./config');
var log         = require('./log')(module);

var models 		 = require('../models');
var AccessToken  = models.AccessToken;
var RefreshToken = models.RefreshToken;




// create OAuth 2.0 server
var aserver = oauth2orize.createServer();

// Generic error handler
var errFn = function (cb, err) {
	if (err) { 
		return cb(err); 
	}
};

// Destroys any old tokens and generates a new access and refresh token
var generateTokens = function (data, done) {
    
	// curries in `done` callback so we don't need to pass it
    var errorHandler = errFn.bind(undefined, done), 
	    refreshToken,
	    refreshTokenValue,
	    token,
        tokenValue;
        
        log.debug(data);

	// RefreshToken.remove(data, errorHandler);
	RefreshToken.destroy({
        where: {
			userID:   data.userId.toString(),
			clientID: data.clientId.toString()			
        }
    	})
        .catch(function (error){
			log.debug(error);
        });

	// AccessToken.remove(data, errorHandler);
	AccessToken.destroy({
        where: {
			userID:   data.userId.toString(),
			clientID: data.clientId.toString()			
        }
    	})
        .catch(function (error){
			log.debug(error);
        });


    tokenValue = crypto.randomBytes(32).toString('hex');
    refreshTokenValue = crypto.randomBytes(32).toString('hex');
  

    data.token = tokenValue;
    token = new AccessToken(data);

    data.token = refreshTokenValue;
    refreshToken = new RefreshToken(data);

    refreshToken.save(errorHandler);

	token.save().then(() => {
		done(null, tokenValue, refreshTokenValue, { 
    		'expires_in': config.get('security:tokenLife') 
    	});
	  }).catch(err => {
		log.debug(err);
		return done(err); 
	  });
	

};

// Exchange username & password for access token.
aserver.exchange(oauth2orize.exchange.password(function(client, username, password, scope, done) {
 

    models.users.findOne({where: {username: username}})
    .then(function(user) {
		
        //if (!user || !user.checkPassword(password)) {
        if (!user ) {
			
			return done(null, false);
		}
		if (user.hashedPassword != password) {
			return done(null, false);
		}
        console.log('All attributes of items:', user.get());
        var model = { 
			userID: user.id,
			userId: user.id,  
            clientId: client.clientId,
            clientID: client.clientId,
		};

		generateTokens(model, done);   
    
  })
  .catch(function(err) {
    // Error handling here
    return done(err); 
  });

}));

// Exchange refreshToken for access token.
aserver.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {

	RefreshToken.findOne({where: 
							{token:    refreshToken, 
							 clientID: client.clientId}
						 })
    .then(function(token) {

		if (!token) { 
			return done(null, false); 
		}
		models.users.findOne({where: {id: parseInt(token.userID)}})
		.then(function(user) {
			if (!user) { return done(null, false); }
			var model = { 
				userId: user.id, 
				userID: user.id,
				clientId: client.clientId,
				clientID: client.clientId,
			};

			generateTokens(model, done);

		}).catch(function(erro) {
			return done(erro); 
		});

	})
	.catch(function(err) {
		// Error handling here
		return done(err); 
	  });

}));


exports.token = [
	
    passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
    //passport.authenticate(['oauth2-client-password'], { session: false }),
	aserver.token(),
	aserver.errorHandler()
];