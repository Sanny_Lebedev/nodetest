// PHONES
var Sequelize = require('sequelize');


module.exports = function(sequelize, DataTypes) {
    var phones = sequelize.define("phones", {
        phone: Sequelize.STRING,  
      }, 
      {
        tableName: 'phones',
        timestamps: true
      }
    );
  


    return phones;
  }