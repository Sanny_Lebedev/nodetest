// PHONES
var Sequelize = require('sequelize');


module.exports = function(sequelize, DataTypes) {
    var AccessToken  = sequelize.define("AccessToken", {
        userID: {
            type:Sequelize.STRING,
            allowNull: false,
            len: [2,150],
            trim: true
        },
        clientID: {
            type:Sequelize.STRING,            
            allowNull: false,
            len: [2,150],
            trim: true
        },
        token: {
            type:Sequelize.STRING,
            unique: true,
            allowNull: false,
            len: [2,150],
            trim: true
        }     
      }, 
      {
        tableName: 'AccessTokens',
        timestamps: true
      }
    );
  
    return AccessToken;
  }