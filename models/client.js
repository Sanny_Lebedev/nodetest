var Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  var client = sequelize.define("client", {
    name: DataTypes.STRING,
    clientId: DataTypes.STRING,
    clientSecret: DataTypes.STRING
  }, 
  {
    tableName: 'Clients',
  });
  return client;
};


