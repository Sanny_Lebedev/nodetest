
var log     = require(__dirname + '/../libs/log')(module);

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(__filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config.json');


let db = null;
 
    if (!db) {
    const sequelize = new Sequelize(config.postgres.uri);

    sequelize
    .authenticate()
    .then(() => {
        log.debug('Connection has been established successfully.');
    })
    .catch(err => {
        log.debug('Unable to connect to the database:', err);
    });

    db = {
        sequelize,
        Sequelize,
        models: {}
       };




fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
    }
module.exports = db;