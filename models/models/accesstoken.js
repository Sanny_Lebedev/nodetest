'use strict';
module.exports = (sequelize, DataTypes) => {
  var AccessToken = sequelize.define('AccessToken', {
    userID: DataTypes.STRING,
    clientID: DataTypes.STRING,
    token: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return AccessToken;
};