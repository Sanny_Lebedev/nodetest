'use strict';
module.exports = (sequelize, DataTypes) => {
  var RefreshToken = sequelize.define('RefreshToken', {
    userID: DataTypes.STRING,
    clientID: DataTypes.STRING,
    token: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
      }
    }
  });
  return RefreshToken;
};