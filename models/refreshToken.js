// PHONES
var Sequelize = require('sequelize');


module.exports = function(sequelize, DataTypes) {
    var RefreshToken = sequelize.define("RefreshToken", {
        userID: {
            type:Sequelize.STRING,
            allowNull: false,
            len: [2,150],
            trim: true
        },
        clientID: {
            type:Sequelize.STRING,            
            allowNull: false,
            len: [2,150],
            trim: true
        },
        token: {
            type:Sequelize.STRING,
            unique: true,
            allowNull: false,
            len: [2,150],
            trim: true
        }  
      }, 
      {
        tableName: 'RefreshTokens',
        timestamps: true
      },
      {
        classMethods: {
            remove: function(data) {
                log.debug(data);
                return true;
            },
        }
      }
    );
  
    return RefreshToken;
  }