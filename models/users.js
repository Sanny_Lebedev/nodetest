var crypto      = require('crypto');
var Sequelize = require('sequelize');
// User


module.exports = function(sequelize, DataTypes) {
    const users = sequelize.define("users", {
        username: {
            type:Sequelize.STRING,
            unique: true,
            allowNull: false,
            len: [2,150],
            trim: true
        },
        hashedPassword: {
            type: Sequelize.STRING,
            allowNull: false,
            len: [2,250],
            trim: true
        },
        salt: {
            type: Sequelize.STRING,
            allowNull: false,
            len: [2,250],
            trim: true
        } 
      }, 
      {
        tableName: 'Users',
        timestamps: true
      },
      {
        classMethods: {
            checkPassword: function(password) {
            return true;
          }
        }
      }
    );
  
    users.encryptPassword = function (password) {
        return crypto.createHmac('sha1', this.salt).update(password).digest('hex');
        //more secure - return crypto.pbkdf2Sync(password, this.salt, 10000, 512).toString('hex'); 
    }
    users.checkPassword = function (password) {
        return this.encryptPassword(password) === this.hashedPassword;
    }



    return users;
  }

 