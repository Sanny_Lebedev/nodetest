var config      = require('../libs/config');
var log         = require('../libs/log')(module);

var models 		 = require('../models');


/* PHONE. */

/* WORK PART */

function isIdUnique (id, done) {
   return models.phones.count({where:{phone:id}})
    .then( function (count) {
          done(count !== 0);
      }
    );

  }

function CheckNumberInParam(number, res) {
    if (!number) {
        return res.json({success: false, message: 'Please enter phone number.'});         
    }  
}






// Checking the phone number in the database 
exports.checknum = function(req, res){
   
    CheckNumberInParam(req.body.number, res);

    isIdUnique(req.body.number, function(isUnique) {
        if (isUnique) {
            return res.json({success: true, message:  'The number is in the database'});
        } 
            return res.json({success: false, message: 'Number does not exist in the database'});
      });

    
 };


 // Adding a phone number to the database
 exports.putnum = function(req, res){

    CheckNumberInParam(req.body.number, res);

    isIdUnique(req.body.number, function(isUnique) {
        if (isUnique) {
            return res.json({success: false, alert: 'danger', message:  'This number is already in the database'});
        } 
            
        
        var data = { phone: req.body.number };        
        newphone = new models.phones(data);
    
        newphone.save()
        .then(function(phone) {            
            return res.json({success: true, alert: 'success', message: 'Number saved successfully'});   
            })
        .catch(err => {
            log.debug(err);
            return done(err); 
          });

        
      });
 };

 // Deleting a phone number from the database
 exports.delnum = function(req, res){
    CheckNumberInParam(req.body.number, res);

    isIdUnique(req.body.number, function(isUnique) {
        if (!isUnique) {
            return res.json({success: false, alert: 'danger',  message:  'This phone number not found'});
        } 
            
        models.phones.destroy( { where: { phone: req.body.number} })
            .then(function(phone) {            
                 return res.json({success: true, alert: 'success', message: 'This phone number was deleted successfully'});   
                })
            .catch(function (error){
                return res.json({success: false, message: error});   
            });
        
      });

   
 };


 // Getting all numbers

 exports.getall = function(req, res){
   //  CheckNumberInParam(req.body.number, res);

   models.phones.findAll({ attributes: ['id','phone'] })
   .then(function(results) {
        
        return res.json({success: true, message: 'The list of numbers was successfully received', numbers:results});   
   })
   .catch(function (error){
        return res.json({success: false, message: error});   
    });


   
 };
/* END WORK PART */


/* GET API.
exports.get_part = function(req, res){
   res.send('GETPART is running');
};

exports.post_part = function(req, res){
   res.send('GETPART is running');
};

exports.getid_part = function(req, res){
   res.send('GETPART is running');
};

exports.putid_part = function(req, res){
   res.send('GETPART is running');
};

exports.delete_part = function(req, res){
   res.send('GETPART is running');
};
/*

exports.get_part = function(req, res) {
    res.send('This is not implemented now');
};

exports.post_part = function(req, res) {
    res.send('This is not implemented now');
);

exports.getid_part = function(req, res) {
    res.send('This is not implemented now');
);

exports.putid_part = function (req, res){
    res.send('This is not implemented now');    
);

exports.delete_part = function (req, res){
    res.send('This is not implemented now');
);
*/