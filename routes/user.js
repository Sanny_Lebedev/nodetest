/* GET users listing. */
exports.list = function(req, res){
  res.send('respond with a resource');
};


exports.userinfo = function(req, res){
  res.json({ 
    user_id: req.user.userId, 
    name: req.user.username, 
    scope: req.authInfo.scope 
  });
};
